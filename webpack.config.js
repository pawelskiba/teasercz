var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
    entry: './entry.js',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.min.js'
    },
    module:{
        loaders:[
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: ['css-loader', 'postcss-loader', 'sass-loader'],
                })
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules$/,
                query:{
                    presets: ['es2015']
                }
            },
            {test: /\.png|jpg|gif$/, loader: 'file-loader' },
            {test: /\.svg|woff|woff2|ttf|eot|otf(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
        ],
    },
    plugins: [
        new ExtractTextPlugin('bundle.min.css'),
        new LiveReloadPlugin(),
    ],
    watch:true,
    devtool: 'source-map',
};
