module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json"),

        uglify: {
            options: {
                mangle: {
                    except: ['jQuery', '$']
                }
            },
            my_target: {
                files: {
                    'assets/js/vendor.min.js': [
                        'assets/js/includes/aos.js',
                    ]
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');

};
