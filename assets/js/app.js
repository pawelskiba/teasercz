// Set the date we're counting down t
var countDownDate = new Date("Aug 27, 2018 10:00:00").getTime();


// Update the count down every 1 secon
var x = setInterval(function () {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    //days
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));

    var days1 = ('0' + days).slice(-2).slice(0, 1);
    document.getElementById('days1').innerHTML = days1;

    var days2 = ('0' + days).slice(-2).slice(1, 2);
    document.getElementById('days2').innerHTML = days2;

    //hours
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

    var hours1 = ('0' + hours).slice(-2).slice(0, 1);
    document.getElementById('hours1').innerHTML = hours1;

    var hours2 = ('0' + hours).slice(-2).slice(1, 2);
    document.getElementById('hours2').innerHTML = hours2;

    //minutes
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));

    var minutes1 = ('0' + minutes).slice(-2).slice(0, 1);
    document.getElementById('minutes1').innerHTML = minutes1;

    var minutes2 = ('0' + minutes).slice(-2).slice(1, 2);
    document.getElementById('minutes2').innerHTML = minutes2;

    //seconds
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    var seconds1 = ('0' + seconds).slice(-2).slice(0, 1);
    document.getElementById('seconds1').innerHTML = seconds1;

    var seconds2 = ('0' + seconds).slice(-2).slice(1, 2);
    document.getElementById('seconds2').innerHTML = seconds2;

    // If the count down is finished, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }

}, 1000);


AOS.init({
    startEvent: 'load',
    easing: 'ease-in-sine',
    once: true,
    disable: function() {
        var maxWidth = 1200;
        return window.innerWidth < maxWidth;
    },
});

function fade(element) {
    var op = 1; // initial opacity
    var timer = setInterval(function () {
        if (op <= 0.1){
            clearInterval(timer);
            element.style.display = 'none';
        }
        element.style.opacity = op;
        element.style.filter = 'alpha(opacity=' + op * 100 + ")";
        op -= op * 0.1;
    }, 10);
}

window.onload = function(){
    fade(document.getElementById('loader'));
}